Please find the ManageTeam react components directory. This is a real example from the Bridge code repository. Please complete a thorough code review of these files, esp considering:
- Would you make any changes to the file naming or structure themselves? Why or why not?
- Can you improve the way data is being passed between these components?

For the purposes of this assignment, assume all json endpoints return exactly the data needed for each component. Please add comments in gitlab directly. Your role is to both improve the underlying code AND to explain why you're suggesting changes, as you would in a real code review.

---

Hi Forrest and team, for code review, please check the [issues menu](https://gitlab.com/adhyapranata/challenges/issues). Cheers!