# Tested on ruby 2.4.0p0

def matcher_regex(pattern, str)
  return false if pattern.length > str.length

  pattern = pattern.split("")

  patterninfo = {}
  norepeat = true

  backrefcount = 1
  regex = ""

  pattern.each do |patternkey|
    patternkey = patternkey.to_sym
    
    if patterninfo.key?(patternkey)
      norepeat = false
      regex += "\\" + patterninfo[patternkey].to_s
    else
      patterninfo[patternkey] = backrefcount
      backrefcount += 1
      regex += "(.+)"
    end
  end

  return true if norepeat

  return str.match?("^#{regex}$")
end

puts "abab, redblueredblue: #{matcher_regex('abab', 'redblueredblue')}"
puts "aaaa, asdfasdfasdfasdf: #{matcher_regex('aaaa', 'asdasdasdasd')}"
puts "aabb, xyzabcxzyabc: #{matcher_regex('aabb', 'xyzabcxzyabc')}"
